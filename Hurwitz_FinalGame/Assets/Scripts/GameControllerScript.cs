﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControllerScript : MonoBehaviour
{
    public GameObject titleScreen;
    public GameObject number1;
    public GameObject number2;
    public GameObject choice1;
    public GameObject choice2;
    public GameObject titleWhite;
    public GameObject titleRed;

    public Text choice1Text;
    public Text choice2Text;

    public Button select1;
    public Button select2;

    public AudioSource[] audioSources;
    public Image[] image;
    public Text[] context;

    private bool TitleScreen;
    private bool Background = false;
    private bool Begin = false;
    private bool A = false;
    private bool A1 = false;
    private bool A1x = false;
    private bool A1y = false;
    private bool A2 = false;
    private bool A2x = false;
    private bool A2y = false;
    private bool B = false;
    private bool B1 = false;
    private bool B1x = false;
    private bool B1y = false;
    private bool B2 = false;
    private bool B2x = false;
    private bool B2y = false;

    void Start()
    {
        TitleScreen = true;
        choice1Text.text = "PRESS 1 OR CLICK BUTTON TO START";
    }

    void Update()
    {
        if (Input.GetKeyDown("1"))
        {
            if (TitleScreen == true)
            {
                Destroy(titleScreen);
                Destroy(titleWhite);
                Destroy(titleRed);
                TitleScreen = false;
                Background = true;
                image[0].enabled = true;
                context[0].enabled = true;
                choice1Text.text = "Open front door.";
                choice2Text.text = "";
                return;
            }
            if (Background == true)
            {
                Background = false;
                image[0].enabled = false;
                context[0].enabled = false;
                audioSources[0].volume = 0f;
                audioSources[1].volume = 1f;
                Begin = true;
                image[1].enabled = true;
                context[1].enabled = true;
                choice1Text.text = "Read the note.";
                choice2Text.text = "Chase after the figure.";
                audioSources[17].Play();
                return;
            }
            
            if (Begin == true)
            {
                Begin = false;
                audioSources[2].volume = 1f;
                A = true;
                image[1].enabled = false;
                context[1].enabled = false;
                image[2].enabled = true;
                image[16].enabled = true;
                context[2].enabled = true;
                choice1Text.text = "Go down to the basement.";
                choice2Text.text = "Go up to your daughter's bedroom.";
                audioSources[20].Play();
                return;
            }
            if (A == true)
            {
                A = false;
                audioSources[3].volume = 1f;
                image[2].enabled = false;
                image[16].enabled = false;
                context[2].enabled = false;
                image[3].enabled = true;
                context[3].enabled = true;
                A1 = true;
                choice1Text.text = "Give your blood to the Widower.";
                choice2Text.text = "Try to free your daughter.";
                audioSources[21].Play();
                return;
            }
            if (A1 == true)
            {
                A1 = false;
                audioSources[4].volume = 1f;
                image[3].enabled = false;
                context[3].enabled = false;
                image[4].enabled = true;
                context[4].enabled = true;
                A1x = true;
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[27].Play();
                return;
            }
            if (A2 == true)
            {
                A2 = false;
                audioSources[7].volume = 1f;
                image[6].enabled = false;
                context[6].enabled = false;
                image[7].enabled = true;
                context[7].enabled = true;
                A2x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[25].Play();
                return;
            }
            if (B == true)
            {
                B = false;
                image[9].enabled = false;
                context[9].enabled = false;
                audioSources[10].volume = 1f;
                image[10].enabled = true;
                context[10].enabled = true;
                B1 = true;
                choice1Text.text = "Call the police.";
                choice2Text.text = "Let the Widower die.";
                audioSources[26].Play();
                return;
            }
            if (B1 == true)
            {
                B1 = false;
                image[10].enabled = false;
                context[10].enabled = false;
                audioSources[11].volume = 1f;
                image[11].enabled = true;
                context[11].enabled = true;
                B1x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[22].Play();
                return;
            }
            if (B2 == true)
            {
                B2 = false;
                image[13].enabled = false;
                context[13].enabled = false;
                audioSources[14].volume = 1f;
                image[14].enabled = true;
                context[14].enabled = true;
                B2x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[28].Play();
                return;
            }
        }








        if (Input.GetKeyDown("2"))
        {
            if (A1x == true || A1y == true || A2x == true || A2y == true || B1x == true || B1y == true || B2x == true || B2y == true)
            {
                SceneManager.LoadScene("Main");
            }

            if (Begin == true)
            {
                Begin = false;
                image[1].enabled = false;
                context[1].enabled = false;
                audioSources[9].volume = 1f;
                image[9].enabled = true;
                context[9].enabled = true;
                B = true;
                choice1Text.text = "Be still.";
                choice2Text.text = "Protect yourself.";
                audioSources[19].Play();
                return;
            }
            if (B == true)
            {
                B = false;
                image[9].enabled = false;
                context[9].enabled = false;
                audioSources[13].volume = 1f;
                image[13].enabled = true;
                context[13].enabled = true;
                B2 = true;
                choice1Text.text = "Speak with the Widower.";
                choice2Text.text = "Kill the Widower";
                audioSources[23].Play();
                return;
            }
            if (B1 == true)
            {
                B1 = false;
                image[10].enabled = false;
                context[10].enabled = false;
                audioSources[12].volume = 1f;
                image[12].enabled = true;
                context[12].enabled = true;
                B1y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[29].Play();
                return;
            }
            if (B2 == true)
            {
                B2 = false;
                image[13].enabled = false;
                context[13].enabled = false;
                audioSources[15].volume = 1f;
                image[15].enabled = true;
                context[15].enabled = true;
                B2y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[24].Play();
                return;
            }
            if (A == true)
            {
                A = false;
                image[2].enabled = false;
                image[16].enabled = false;
                context[2].enabled = false;
                audioSources[6].volume = 1f;
                image[6].enabled = true;
                context[6].enabled = true;
                A2 = true;
                choice1Text.text = "Turn yourself in.";
                choice2Text.text = "Remain the Widower.";
                audioSources[16].Play();
                return;
            }
            if (A1 == true)
            {
                A1 = false;
                image[3].enabled = false;
                context[3].enabled = false;
                audioSources[5].volume = 1f;
                image[5].enabled = true;
                context[5].enabled = true;
                A1y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[30].Play();
                return;
            }
            if (A2 == true)
            {
                A2 = false;
                image[6].enabled = false;
                context[6].enabled = false;
                audioSources[8].volume = 1f;
                image[8].enabled = true;
                context[8].enabled = true;
                A2y = true;
                Destroy(select1);
                Destroy(number1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[18].Play();
                return;
            }
        }
    }

























    public void ButtonPressed (int buttonNum)
    {
        if (buttonNum == 1)
        {
            if (TitleScreen == true)
            {
                Destroy(titleScreen);
                Destroy(titleWhite);
                Destroy(titleRed);
                TitleScreen = false;
                Background = true;
                image[0].enabled = true;
                context[0].enabled = true;
                choice1Text.text = "Open front door.";
                choice2Text.text = "";
                return;
            }
            if (Background == true)
            {
                Background = false;
                image[0].enabled = false;
                context[0].enabled = false;
                audioSources[0].volume = 0f;
                audioSources[1].volume = 1f;
                Begin = true;
                image[1].enabled = true;
                context[1].enabled = true;
                choice1Text.text = "Read the note.";
                choice2Text.text = "Chase after the figure.";
                audioSources[17].Play();
                return;
            }

            if (Begin == true)
            {
                Begin = false;
                audioSources[2].volume = 1f;
                A = true;
                image[1].enabled = false;
                context[1].enabled = false;
                image[2].enabled = true;
                image[16].enabled = true;
                context[2].enabled = true;
                choice1Text.text = "Go down to the basement.";
                choice2Text.text = "Go up to your daughter's bedroom.";
                audioSources[20].Play();
                return;
            }
            if (A == true)
            {
                A = false;
                audioSources[3].volume = 1f;
                image[2].enabled = false;
                image[16].enabled = false;
                context[2].enabled = false;
                image[3].enabled = true;
                context[3].enabled = true;
                A1 = true;
                choice1Text.text = "Give your blood to the Widower.";
                choice2Text.text = "Try to free your daughter.";
                audioSources[21].Play();
                return;
            }
            if (A1 == true)
            {
                A1 = false;
                audioSources[4].volume = 1f;
                image[3].enabled = false;
                context[3].enabled = false;
                image[4].enabled = true;
                context[4].enabled = true;
                A1x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[27].Play();
                return;
            }
            if (A2 == true)
            {
                A2 = false;
                audioSources[7].volume = 1f;
                image[6].enabled = false;
                context[6].enabled = false;
                image[7].enabled = true;
                context[7].enabled = true;
                A2x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[25].Play();
                return;
            }
            if (B == true)
            {
                B = false;
                image[9].enabled = false;
                context[9].enabled = false;
                audioSources[10].volume = 1f;
                image[10].enabled = true;
                context[10].enabled = true;
                B1 = true;
                choice1Text.text = "Call the police.";
                choice2Text.text = "Let the Widower die.";
                audioSources[26].Play();
                return;
            }
            if (B1 == true)
            {
                B1 = false;
                image[10].enabled = false;
                context[10].enabled = false;
                audioSources[11].volume = 1f;
                image[11].enabled = true;
                context[11].enabled = true;
                B1x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[22].Play();
                return;
            }
            if (B2 == true)
            {
                B2 = false;
                image[13].enabled = false;
                context[13].enabled = false;
                audioSources[14].volume = 1f;
                image[14].enabled = true;
                context[14].enabled = true;
                B2x = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[28].Play();
                return;
            }
        }







        if (buttonNum == 2)
        {
            if (A1x == true || A1y == true || A2x == true || A2y == true || B1x == true || B1y == true || B2x == true || B2y == true)
            {
                SceneManager.LoadScene("Main");
            }

            if (Begin == true)
            {
                Begin = false;
                image[1].enabled = false;
                context[1].enabled = false;
                audioSources[9].volume = 1f;
                image[9].enabled = true;
                context[9].enabled = true;
                B = true;
                choice1Text.text = "Be still.";
                choice2Text.text = "Protect yourself.";
                audioSources[19].Play();
                return;
            }
            if (B == true)
            {
                B = false;
                image[9].enabled = false;
                context[9].enabled = false;
                audioSources[13].volume = 1f;
                image[13].enabled = true;
                context[13].enabled = true;
                B2 = true;
                choice1Text.text = "Speak with the Widower.";
                choice2Text.text = "Kill the Widower";
                audioSources[23].Play();
                return;
            }
            if (B1 == true)
            {
                B1 = false;
                image[10].enabled = false;
                context[10].enabled = false;
                audioSources[12].volume = 1f;
                image[12].enabled = true;
                context[12].enabled = true;
                B1y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[29].Play();
                return;
            }
            if (B2 == true)
            {
                B2 = false;
                image[13].enabled = false;
                context[13].enabled = false;
                audioSources[15].volume = 1f;
                image[15].enabled = true;
                context[15].enabled = true;
                B2y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[24].Play();
                return;
            }
            if (A == true)
            {
                A = false;
                image[2].enabled = false;
                image[16].enabled = false;
                context[2].enabled = false;
                audioSources[6].volume = 1f;
                image[6].enabled = true;
                context[6].enabled = true;
                A2 = true;
                choice1Text.text = "Turn yourself in.";
                choice2Text.text = "Remain the Widower.";
                audioSources[16].Play();
                return;
            }
            if (A1 == true)
            {
                A1 = false;
                image[3].enabled = false;
                context[3].enabled = false;
                audioSources[5].volume = 1f;
                image[5].enabled = true;
                context[5].enabled = true;
                A1y = true;
                Destroy(select1);
                Destroy(number1);
                Destroy(choice1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[30].Play();
                return;
            }
            if (A2 == true)
            {
                A2 = false;
                image[6].enabled = false;
                context[6].enabled = false;
                audioSources[8].volume = 1f;
                image[8].enabled = true;
                context[8].enabled = true;
                A2y = true;
                Destroy(select1);
                Destroy(number1);
                choice1Text.text = "";
                choice2Text.text = "RESTART";
                audioSources[18].Play();
                return;
            }
        }
    }
}
   